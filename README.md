# intro-to-vue-js

Página de productos para aprender conceptos básicos de Vue.js

## Conceptos a desarrollar

Vue Instance, Reactivity, Attribute Binding, Conditional Rendering, List Rendering, Event Handling, Class and Style Binding, Computed Properties, Components, Communicating Events, Forms, Tabs 

## Tecnologías

Lenguaje: Javascript

Framework: Vue.js

## Uso
Para ejecutar la aplicación ejecutar el siguiente comando:

```bash
1) Abrir el archivo index.html en algún navegador web
```

## Referencias
[Curso *Intro to Vue.js* - vuejs.org](https://www.vuemastery.com/courses/intro-to-vue-js/)
