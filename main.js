/******************************************************************
 *                            EVENT-BUS                            
 *****************************************************************/

var eventBus = new Vue() 

/******************************************************************
 *                             PRODUCT                             
 *****************************************************************/
Vue.component('product', {
  props: {
    premium: {
      type: Boolean,
      required: true
    }
  },
  template: `
  <fieldset>
    <legend>PRODUCT</legend>
    <div class="product">
      <div class="product-data">
        <div class="product-image">
          <img :src="image" /> 
        </div>
        <div class="product-info">
          <h1>{{ title }}</h1>
          <p v-if="inStock">In Stock</p>
          <p v-else>Out of Stock</p>
          <p>{{ sale }}</p>
          <info-tabs :shipping="shipping" :details="details"></info-tabs>
          <div  class="color-box"
                v-for="(variant, index) in variants" :key="variant.variantId"
                :style="{ backgroundColor: variant.variantColor }"
                @mouseover="updateProduct(index)">
          </div>
          <button v-on:click="addToCart" 
                  :disabled="!inStock"
                  :class="{ disabledButton: !inStock }">
                  Add to cart
          </button>
        </div>
      </div>  
      <div>
        <review-tabs :reviews="reviews"></review-tabs>
      </div>
    </div>
  </fieldset>`,
  data () {
    return {
      brand: 'Vue Mastery',
      product: 'Socks',
      selectedVariant: 0,
      onSale: true,
      details: ['80 cotton', '20% polyester', 'Gender-neutral'],
      variants: [
        {
          variantId: 2234,
          variantColor: 'green',
          variantImage: './assets/sock-green.jpg',
          variantQuantity: 10
        }, {
          variantId: 2235,
          variantColor: 'blue',
          variantImage: './assets/sock-blue.jpg',
          variantQuantity: 0
        }
      ],
      sizes: ['S', 'M', 'L', 'XL', 'XXL', 'XXXL'],
      reviews: []
    }
  },
  methods: {
    addToCart() {
      this.$emit('add-to-cart', this.variants[this.selectedVariant].variantId)
    },
    updateProduct(index) {
      console.log('COLOR', index);
      
      this.selectedVariant = index
    }
  },
  computed: {
    title() {
      return `${this.brand} ${this.product}`
    },
    image() {
      return this.variants[this.selectedVariant].variantImage
    },
    inStock() {
      return this.variants[this.selectedVariant].variantQuantity > 0
    },
    sale() {
      let text = 'are not on sale'
      if (this.onSale) {
        text = 'are on sale!'
      }
      return `${this.title} ${text}`
    },
    shipping() {
      if (this.premium) {
        return "Free"
      }
      return 2.99
    }
  },
  mounted() {
    eventBus.$on('review-submitted', productReview => {
      this.reviews.push(productReview)
    })
  }

})

/******************************************************************
 *                         INFO-TABS                         
 *****************************************************************/
Vue.component('info-tabs', {
  props: {
    shipping: {
      required: true
    },
    details: {
      type: Array,
      required: true
    }
  },
  template: `
    <fieldset>
      <legend>INFO-TABS</legend>
      <div>
        <span class="tab"
              :class="{activeTab: selectedTab === tab}"
              v-for="(tab, index) in tabs" :key="index"
              @click="selectedTab = tab">
              {{tab}}
        </span>
        <div v-show="selectedTab === 'Shipping'">
          <p>{{ shipping }}</p>
        </div>
        <div v-show="selectedTab === 'Details'">
          <product-detail :details="details"></product-detail>
        </div>
      </div>
    </fieldset>
  `,
  data() {
    return {
      tabs: ['Shipping', 'Details'],
      selectedTab: 'Shipping'
    }
  }
})

/******************************************************************
 *                         PRODUCT-DETAIL                         
 *****************************************************************/
Vue.component('product-detail', {
  props: {
    details: {
      type: Array,
      required: true
    }
  },
  template: `
      <fieldset>
        <legend>PRODUCT DETAIL</legend>
        <ul>
          <li v-for="detail in details">{{ detail }}</li>
        </ul>
      </fieldset>
      `
})

/******************************************************************
 *                          REVIEW-TABS                          
 *****************************************************************/
Vue.component('review-tabs', {
  props: {
    reviews: {
      type: Array,
      required: true
    }
  },
  template: `
  <fieldset>
    <legend>REVIEW-TABS</legend>
    <div>
      <span class="tab"
            :class="{activeTab: selectedTab === tab}"
            v-for="(tab, index) in tabs" :key="index"
            @click="selectedTab = tab">
            {{tab}}
      </span>
      <div v-show="selectedTab === 'Reviews'">
        <p v-if="!reviews.length">There are o reviews yet</p>
        <ul>
          <li v-for="review in reviews">
            <p>Name: {{review.name}}</p>
            <p>Review: {{review.review}}</p>
            <p>Rating: {{review.rating}}</p>
          </li>
        </ul>
      </div>
      <product-review  v-show="selectedTab === 'Make a Review'"></product-review>
    </div>
  </fieldset>
  `,
  data() {
    return {
      tabs: ['Reviews', 'Make a Review'],
      selectedTab: 'Reviews'
    }
  }
})

/******************************************************************
 *                         PRODUCT-REVIEW                         
 *****************************************************************/
Vue.component('product-review', {
  template: `
  <fieldset>
    <legend>PRODUCT-REVIEW</legend>
    <form class="review-form" @submit.prevent="onSubmit">
      <p v-if="errors.length">
        <b>Please correct the following error(s):</b>
        <ul>
          <li v-for="error in errors">{{error}}</li>
        </ul>
      </p>
      <p>
        <label for="name">Name:</label>
        <input id="name" v-model="name" placeholder="name">
      </p>
      <p>
        <label for="review">Review:</label>
        <textarea id="review" v-model="review" placeholder="description"></textarea>
      </p>
      <p>
        <label for="rating">Rating:</label>
        <select id="rating" v-model.number="rating">
          <option>5</option>
          <option>4</option>
          <option>3</option>
          <option>2</option>
          <option>1</option>
        </select>
      </p>
      <p>Would you recommend this product?</p>
      <div>
        
        <label for="yes">Yes<input type="radio" id="yes" name="recommend" v-model="recommend" value="yes"></label>
        <label for="no">No<input type="radio" id="no" name="recommend" v-model="recommend" value="no"></label>
      </div>
      <p>
        <input type="submit" value="Submit">  
      </p>
    </form>
  </fieldset>`,
  data () {
    return {
      name: null,
      review: null,
      rating: null,
      recommend: 'yes',
      errors: []
    }
  },
  methods: {
    onSubmit() {
      this.errors = []
      if (this.name && this.review && this.rating && this.recommend) {
        let productReview = {
          name: this.name,
          review: this.review,
          rating: this.rating,
          recommend: this.recommend
        }
        eventBus.$emit('review-submitted', productReview)
        this.name = null
        this.review = null
        this.rating = null,
        this.recommend = 'yes'
      } else {
        if(!this.name) this.errors.push('Name required')
        if(!this.review) this.errors.push('Review required')
        if(!this.rating) this.errors.push('Rating required')
        if(!this.recommend) this.errors.push('Recommendation required')
      }
    }
  }
})

/******************************************************************
 *                         NEW-VUE                         
 *****************************************************************/
var app = new Vue({
  el: '#app',
  data: {
    premium: false,
    cart: []
  },
  methods: {
    updateCart(id) {
      this.cart.push(id)
    }
  }
})